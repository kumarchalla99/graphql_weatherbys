const mysql = require('mysql2');

(function testConnection() {
  mysqlConnection = mysql.createConnection({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    database: 'chat_app',
    password: 'xxxxxxx',
  });
  console.log(mysqlConnection);
})();
