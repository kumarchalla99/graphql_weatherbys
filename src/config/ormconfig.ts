import { DataSource } from "typeorm";
import { config as dotenvConfig } from "dotenv";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";

dotenvConfig({
  path: `${process.cwd()}/src/config/.env.${
    process.env.NODE_ENV || "development"
  }`,
});
export const typeOrmModuleOptions: TypeOrmModuleOptions = {
  type: "mysql",
  host: process.env.DATABASE_HOST,
  port: parseInt(<string>process.env.DATABASE_PORT),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_DB,
  entities: [__dirname + "../**/*.model.{js,ts}"],
  synchronize: false,
  autoLoadEntities: true,
};

export const OrmConfig = new DataSource({
  type: "mysql",
  host: process.env.DATABASE_HOST,
  port: parseInt(<string>process.env.DATABASE_PORT),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_DB,
  entities: [__dirname + "../**/*.model.{js,ts}"],
  synchronize: false,
  migrations: ["./migrations/*{.ts,.js}"],
  migrationsTableName: "migrations_TypeORM",
});
// export default OrmConfig;
