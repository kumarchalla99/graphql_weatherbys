import { NestFactory, Reflector } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ConfigService } from "@nestjs/config";
import { ClassSerializerInterceptor } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const configService = app.get(ConfigService);
  await app.listen(configService.get("PORT"));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  console.log("server is running on PORT", configService.get("PORT"));
}
bootstrap();
