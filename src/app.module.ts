import { Module } from "@nestjs/common";
import { AppResolver } from "./app.resolver";
import { AppService } from "./app.service";
import { GraphQLModule } from "@nestjs/graphql";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AddressModule } from "./address/address.module";
import { GraphQLError } from "graphql";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { validate } from "./env.validation";
import { customErrorFormat } from "./error/globalError";
import { ErrorInterface } from "./error/error";
import { typeOrmModuleOptions } from "./config/ormconfig";
import { ProductModule } from "./product/product.module";
import { CustomerModule } from "./customer/customer.module";
import { CustomerOrderModule } from "./customer-order/customer-order.module";

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      formatError: (error: GraphQLError): ErrorInterface => {
        return customErrorFormat(error);
      },
      context: ({ req }) => ({ req }),
      sortSchema: true,
      autoSchemaFile: true,
      debug: true,
      playground: true,
    }),
    ConfigModule.forRoot({
      envFilePath: `${process.cwd()}/src/config/.env.${
        process.env.NODE_ENV || "development"
      }`,
      validate,
      isGlobal: true,
      expandVariables: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      // useFactory: async (configService: ConfigService) => ({
      //   type: 'mysql',
      //   host: configService.get('DATABASE_HOST'),
      //   port: configService.get('DATABASE_PORT'),
      //   username: configService.get('DATABASE_USERNAME'),
      //   password: configService.get('DATABASE_PASSWORD'),
      //   database: configService.get('DATABASE_DB'),
      //   entities: [__dirname + '../**/*.model.{js,ts}'],
      //   synchronize: false,
      //   autoLoadEntities: true,
      //   migrations: ['/migrations/*{.ts,.js}'],
      // }),
      // inject: [ConfigService],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        ...typeOrmModuleOptions,
      }),
    }),
    AddressModule,
    CustomerModule,
    CustomerOrderModule,
    ProductModule,
  ],
  controllers: [],
  providers: [AppResolver, AppService],
})
export class AppModule {}
