import HttpException from './httpException';
import { errorType } from './constants';
class BadRequestException extends HttpException {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(message?: string, _code?: string) {
    super(
      errorType.BADREQUEST.statusCode,
      message || 'Invalid Request',
      'BAD REQUEST',
    );
  }
}

export default BadRequestException;
