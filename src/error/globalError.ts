import { GraphQLFormattedError } from 'graphql';
import { ErrorInterface } from './error';

export function customErrorFormat(
  error: GraphQLFormattedError,
): ErrorInterface {
  return {
    message: error.extensions?.response?.message || error.message,
    code: error.extensions.code || 'INTERNAL_SERVER_ERROR',
    status:
      error.extensions?.status || error.extensions?.response?.statusCode || 500,
  };
}
