export interface ErrorInterface {
  message: string;
  status: number | unknown;
  code: string | undefined | unknown;
}
