import { GraphQLError } from 'graphql';

class HttpException extends GraphQLError {
  status: number;

  message: string;

  // eslint-disable-next-line @typescript-eslint/ban-types
  extensions: {};

  constructor(status: number, message: string, code?: string) {
    super(message);
    this.message = message;
    this.extensions = { code: code, status: status };
  }
}

export default HttpException;
