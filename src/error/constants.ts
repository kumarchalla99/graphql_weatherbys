const errorName = {
  UNAUTHORIZED: 'UNAUTHORIZED',
  BADREQUEST: 'BADREQUEST',
};

const errorType = {
  UNAUTHORIZED: {
    message: 'Authentication is needed to get requested response.',
    statusCode: 401,
  },
  BADREQUEST: {
    message: 'Invalid Request',
    statusCode: 400,
  },
};
export { errorName, errorType };
