import HttpException from './httpException';
import { errorName, errorType } from './constants';
class UnauthorizedException extends HttpException {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(message?: string, _code?: string) {
    super(
      errorType.UNAUTHORIZED.statusCode,
      message || 'Not authorized',
      'UNAUTHORIZED_ERROR',
    );
  }
}

export default UnauthorizedException;
