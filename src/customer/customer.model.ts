import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { IsNotEmpty, IsOptional, ValidateIf } from "class-validator";
import { AddressEntity } from "src/address/address.model";
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";

@ObjectType({ description: "customer" })
@Entity("customer")
export class CustomerEntity {
  @Field({ nullable: false, name: "custId" }) //if we want to map different name we can go with name
  @PrimaryGeneratedColumn({ name: "cust_id" })
  custId: number;

  @Field({ nullable: false, name: "firstName" })
  @Column({ nullable: false, name: "first_name" })
  firstName: string;

  @Field({ nullable: false, name: "lastName" })
  @Column({ length: 250, name: "last_name" })
  lastName: string;

  @Field({ nullable: false, name: "gender" })
  @Column({ length: 250, name: "gender" })
  gender: string;

  @Field({ nullable: false, name: "email" })
  @Column({ length: 100, name: "email" })
  email: string;

  @Field({ nullable: false, name: "landLine" })
  @Column({ name: "landLine" })
  landLine: number;

  @Field({ nullable: false, name: "mobile" })
  @Column({ name: "mobile" })
  mobile: number;

  @Field({ nullable: false, name: "addressId" })
  @OneToMany((type) => AddressEntity, (address) => address.id)
  addressId: number;
}

@InputType()
export class CustomerInput {
  @ValidateIf((o) => o.otherProperty === "value")
  @Field({ nullable: false })
  @IsNotEmpty()
  firstName: string;

  @Field({ nullable: false, name: "lastName" })
  @IsNotEmpty()
  lastName: string;

  @Field({ nullable: false, name: "gender" })
  @IsNotEmpty()
  gender: string;

  @Field({ nullable: false, name: "email" })
  @IsNotEmpty()
  email: string;

  @Field({ nullable: false, name: "landLine" })
  @IsNotEmpty()
  landLine: number;

  @Field({ nullable: false, name: "mobile" })
  @IsNotEmpty()
  mobile: number;

  @IsOptional()
  @Field({ nullable: false, name: "addressId" })
  @IsNotEmpty()
  addressId: number;
}
