import { Injectable } from "@nestjs/common";
import { CustomerEntity, CustomerInput } from "./customer.model";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import BadRequestException from "src/error/BadRequestException";

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(CustomerEntity)
    private addressRepository: Repository<CustomerEntity>
  ) {}

  async findAllCustomers(): Promise<CustomerEntity[]> {
    return await this.addressRepository.find();
  }

  async findCustomer(userInput): Promise<CustomerEntity> {
    return await this.addressRepository.findOne({
      where: { ...userInput },
    });
  }

  async addCustomer(customerData: CustomerInput) {
    const adrs = await this.findCustomer({ email: customerData.email });
    if (adrs) throw new BadRequestException("user already exists");
    if (Object.values(customerData).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");

    const savedAdrs = await this.addressRepository.save(customerData);
    return savedAdrs;
  }

  async updateCustomer(id, customerData: CustomerInput) {
    const user: CustomerEntity = await this.findCustomer({
      email: customerData.email,
    });
    if (!user) throw new BadRequestException("user not exists");
    if (Object.values(customerData).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");
    const res = await this.addressRepository.update(
      { custId: id },
      customerData
    );
    if (res) return res;
  }

  async deleteCustomer(custID: number) {
    const user: CustomerEntity = await this.findCustomer({
      id: custID,
    });
    if (!user) throw new BadRequestException("user not exists");
    const res = await this.addressRepository.delete(user);
    if (res) return res;
  }
}
