import { Args, Query, Mutation, Resolver, Context } from "@nestjs/graphql";
import { CustomerEntity, CustomerInput } from "./customer.model";
import { CustomerService } from "./customer.service";
import BadRequestException from "src/error/BadRequestException";
@Resolver()
export class CustomerResolver {
  constructor(private readonly customerService: CustomerService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [CustomerEntity])
  async getAllCustomers() {
    return this.customerService.findAllCustomers();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => CustomerEntity, { nullable: true })
  async getCustomer(
    @Args("customerId", { type: () => Number }) customerId: number
  ) {
    return this.customerService.findCustomer(customerId);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  @Mutation((returns) => CustomerEntity)
  async saveCustomer(@Args("customerData") customerData: CustomerInput) {
    return this.customerService.addCustomer(customerData);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => CustomerEntity)
  async updateCustomer(
    @Args("custId") custId: number,
    @Args("updatedCustomerData") updatedCustomerData: CustomerInput,
    @Context("req") req
  ) {
    try {
      return this.customerService.updateCustomer(custId, updatedCustomerData);
    } catch (e) {
      throw e;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => CustomerEntity)
  async deleteCustomer(@Args("custId") custId: number, @Context("req") req) {
    try {
      if (!custId) throw new BadRequestException(" id cannot be empty");
      return this.customerService.deleteCustomer(custId);
    } catch (e) {
      throw e;
    }
  }
}
