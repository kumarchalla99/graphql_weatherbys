import { CustomerService } from "./customer.service";
import { CustomerResolver } from "./customer.resolver";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CustomerEntity } from "./customer.model";

@Module({
  imports: [TypeOrmModule.forFeature([CustomerEntity])],
  providers: [CustomerResolver, CustomerService],
  exports: [],
})
export class CustomerModule {}
