import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { IsNotEmpty, ValidateIf } from "class-validator";
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";

@ObjectType({ description: "product" })
@Entity("product")
export class ProductEntity {
  @Field({ nullable: false, name: "pId" }) //if we want to map different name we can go with name
  @PrimaryGeneratedColumn({ name: "p_id" })
  pId: number;

  @Field({ nullable: false, name: "descripFon" })
  @Column({ name: "descrip_fon", length: 255 })
  descripFon: string;

  @Field({ nullable: false, name: "unitPrice" })
  @Column({ name: "unit_price" })
  unitPrice: number;

  @Field({ nullable: false, name: "availableQty" })
  @Column({ name: "available_qty" })
  availableQty: number;
}

@InputType()
export class ProductInput {
  @ValidateIf((o) => o.otherProperty === "value")
  @Field({ nullable: false, name: "descripFon" })
  @IsNotEmpty()
  descripFon: string;

  @Field({ nullable: false, name: "unitPrice" })
  @IsNotEmpty()
  unitPrice: number;

  @Field({ nullable: false, name: "availableQty" })
  @IsNotEmpty()
  availableQty: number;
}
