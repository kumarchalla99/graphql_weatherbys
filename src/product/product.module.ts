import { Module } from "@nestjs/common";
import { ProductResolver } from "./product.resolver";
import { ProductEntity } from "./product.model";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ProductService } from "./product.service";

@Module({
  imports: [TypeOrmModule.forFeature([ProductEntity])],
  providers: [ProductResolver, ProductService],
  exports: [],
})
export class ProductModule {}
