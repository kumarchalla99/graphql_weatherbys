import { Args, Query, Mutation, Resolver, Context } from "@nestjs/graphql";
import BadRequestException from "src/error/BadRequestException";
import { ProductService } from "./product.service";
import { ProductEntity, ProductInput } from "./product.model";

@Resolver()
export class ProductResolver {
  constructor(private readonly prodcutService: ProductService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [ProductEntity])
  async getAllProducts() {
    return this.prodcutService.findAllProducts();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => ProductEntity, { nullable: true })
  async getProdcut(
    @Args("productId", { type: () => Number }) productId: number
  ) {
    return this.prodcutService.findProduct(productId);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  @Mutation((returns) => ProductEntity)
  async saveProduct(@Args("product") product: ProductInput) {
    return this.prodcutService.addProduct(product);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => ProductEntity)
  async updateProduct(
    @Args("productId") productId: number,
    @Args("product") product: ProductInput,
    @Context("req") req
  ) {
    try {
      return this.prodcutService.updateProduct(productId, product);
    } catch (e) {
      throw e;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => ProductEntity)
  async deleteProduct(
    @Args("productId") productId: number,
    @Context("req") req
  ) {
    try {
      if (!productId) throw new BadRequestException(" id cannot be empty");
      return this.prodcutService.deleteProduct(productId);
    } catch (e) {
      throw e;
    }
  }
}
