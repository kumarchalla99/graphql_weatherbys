import { Injectable, Scope } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import BadRequestException from "src/error/BadRequestException";
import { ProductEntity, ProductInput } from "./product.model";

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductEntity)
    private addressRepository: Repository<ProductEntity>
  ) {}

  async findAllProducts(): Promise<ProductEntity[]> {
    return await this.addressRepository.find();
  }

  async findProduct(userInput): Promise<ProductEntity> {
    return await this.addressRepository.findOne({
      where: { ...userInput },
    });
  }

  async addProduct(product: ProductInput) {
    const adrs = await this.findProduct({ descripFon: product.descripFon });
    if (adrs) throw new BadRequestException("user already exists");
    if (Object.values(product).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");

    const savedAdrs = await this.addressRepository.save(product);
    return savedAdrs;
  }

  async updateProduct(productId, customerData: ProductInput) {
    const user: ProductInput = await this.findProduct({
      pId: productId,
    });
    if (!user) throw new BadRequestException("user not exists");
    if (Object.values(customerData).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");
    const res = await this.addressRepository.update(
      { pId: productId },
      customerData
    );
    if (res) return res;
  }

  async deleteProduct(productId: number) {
    const user: ProductInput = await this.findProduct({
      id: productId,
    });
    if (!user) throw new BadRequestException("user not exists");
    const res = await this.addressRepository.delete(user);
    if (res) return res;
  }
}
