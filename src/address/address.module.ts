import { AddressService } from "./address.service";
import { AddressResolver } from "./address.resolver";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AddressEntity } from "./address.model";

@Module({
  imports: [TypeOrmModule.forFeature([AddressEntity])],
  providers: [AddressResolver, AddressService],
  exports: [],
  controllers: [],
})
export class AddressModule {}
