import { Injectable, Scope } from "@nestjs/common";
import { AddressEntity, AddressInput } from "./address.model";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import BadRequestException from "src/error/BadRequestException";

@Injectable()
export class AddressService {
  private userDataGlobal: any;
  constructor(
    @InjectRepository(AddressEntity)
    private addressRepository: Repository<AddressEntity>
  ) {}

  async findAllAddress(): Promise<AddressEntity[]> {
    return await this.addressRepository.find();
  }

  async findAddress(userInput): Promise<AddressEntity> {
    return await this.addressRepository.findOne({
      where: { ...userInput },
    });
  }

  async addAddress(address: AddressInput) {
    const adrs = await this.findAddress({ addNo: address.addNo });
    if (adrs) throw new BadRequestException("user already exists");
    if (Object.values(address).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");

    const savedAdrs = await this.addressRepository.save(address);
    return savedAdrs;
  }

  async updateAddress(id, addressData: AddressInput) {
    const user: AddressEntity = await this.findAddress({
      addNo: addressData.addNo,
    });
    if (!user) throw new BadRequestException("user not exists");
    if (Object.values(addressData).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");
    const res = await this.addressRepository.update({ id: id }, addressData);
    if (res) return res;
  }

  async deleteAddress(adrsId: number) {
    const user: AddressEntity = await this.findAddress({
      id: adrsId,
    });
    if (!user) throw new BadRequestException("user not exists");
    const res = await this.addressRepository.delete(user);
    if (res) return res;
  }
}
