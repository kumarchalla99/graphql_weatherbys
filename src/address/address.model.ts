import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { Exclude } from "class-transformer";
import { IsNotEmpty, IsOptional, ValidateIf } from "class-validator";
import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@ObjectType({ description: "address" })
@Entity("address")
export class AddressEntity {
  @Field({ nullable: false, name: "id" }) //if we want to map different name we can go with name
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;

  @Field({ nullable: false, name: "addNo" })
  @Column({ nullable: false, name: "add_no" })
  addNo: number;

  @Field({ nullable: false, name: "addrLine1" })
  @Column({ length: 250, name: "addr_line1" })
  addrLine1: string;

  @Field({ nullable: false, name: "addrLine2" })
  @Column({ length: 250, name: "addr_line2" })
  addrLine2: string;

  @Field({ nullable: false, name: "city" })
  @Column({ length: 100, name: "city" })
  city: string;

  @Field({ nullable: false, name: "postcode" })
  @Column({ name: "postcode" })
  postcode: number;

  @Field({ nullable: false, name: "country" })
  @Column({ name: "country", length: 50 })
  country: string;
}

@InputType()
export class AddressInput {
  @ValidateIf((o) => o.otherProperty === "value")
  @Field({ nullable: false })
  @IsNotEmpty()
  addNo: number;

  @Field({ nullable: false, name: "addrLine1" })
  @IsNotEmpty()
  addrLine1: string;

  @Field({ nullable: false, name: "addrLine2" })
  @IsNotEmpty()
  addrLine2: string;

  @Field({ nullable: false, name: "city" })
  @IsNotEmpty()
  city: string;

  @Field({ nullable: false, name: "postcode" })
  @IsNotEmpty()
  postcode: number;

  @Field({ nullable: false, name: "country" })
  @IsNotEmpty()
  country: string;
}
