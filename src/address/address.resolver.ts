import { Args, Query, Mutation, Resolver, Context } from "@nestjs/graphql";
import { AddressEntity, AddressInput } from "./address.model";
import { AddressService } from "./address.service";
import BadRequestException from "src/error/BadRequestException";
@Resolver()
export class AddressResolver {
  constructor(private readonly addressService: AddressService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [AddressEntity])
  async getAllAddress() {
    return this.addressService.findAllAddress();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => AddressEntity, { nullable: true })
  async getAddress(
    @Args("addressId", { type: () => Number }) addressId: number
  ) {
    return this.addressService.findAddress(addressId);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  @Mutation((returns) => AddressEntity)
  async saveAddress(@Args("address") address: AddressInput) {
    return this.addressService.addAddress(address);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => AddressEntity)
  async updateAddress(
    @Args("id") id: number,
    @Args("updatedAddress") updatedAddress: AddressInput,
    @Context("req") req
  ) {
    try {
      return this.addressService.updateAddress(id, updatedAddress);
    } catch (e) {
      throw e;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => AddressEntity)
  async deleteAddress(@Args("id") id: number, @Context("req") req) {
    try {
      if (!id) throw new BadRequestException(" id cannot be empty");
      return this.addressService.deleteAddress(id);
    } catch (e) {
      throw e;
    }
  }
}
