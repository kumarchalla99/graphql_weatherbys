import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CustomerOrderEntity } from "./customer-order.model";
import { CustomerOrderResolver } from "./customer-order.resolver";
import { CustomerOrderService } from "./customer-order.service";

@Module({
  imports: [TypeOrmModule.forFeature([CustomerOrderEntity])],
  providers: [CustomerOrderResolver, CustomerOrderService],
  exports: [],
})
export class CustomerOrderModule {}
