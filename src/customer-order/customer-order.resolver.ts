import { Args, Context, Mutation, Query, Resolver } from "@nestjs/graphql";
import BadRequestException from "src/error/BadRequestException";
import { CustomerOrderService } from "./customer-order.service";
import {
  CustomerOrderEntity,
  CustomerOrderInput,
} from "./customer-order.model";

@Resolver()
export class CustomerOrderResolver {
  constructor(private readonly customerOrderService: CustomerOrderService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [CustomerOrderEntity])
  async getAllCustomerOrders() {
    return this.customerOrderService.findAllCustomerOrders();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => CustomerOrderEntity, { nullable: true })
  async getCustomerOrder(
    @Args("customerOrderId", { type: () => Number }) customerOrderId: number
  ) {
    return this.customerOrderService.findCustomerOrder(customerOrderId);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  @Mutation((returns) => CustomerOrderEntity)
  async saveCustomerOrder(
    @Args("customerOrder") customerOrder: CustomerOrderInput
  ) {
    return this.customerOrderService.addCustomerOrder(customerOrder);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => CustomerOrderEntity)
  async updateCustomerOrder(
    @Args("customerOrderId") customerOrderId: number,
    @Args("customerOrder") customerOrder: CustomerOrderInput,
    @Context("req") req
  ) {
    try {
      return this.customerOrderService.updateCustomerOrder(
        customerOrderId,
        customerOrder
      );
    } catch (e) {
      throw e;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => CustomerOrderEntity)
  async deleteCustomerOrder(
    @Args("customerOrderId") customerOrderId: number,
    @Context("req") req
  ) {
    try {
      if (!customerOrderId)
        throw new BadRequestException(" id cannot be empty");
      return this.customerOrderService.deleteCustomerOrder(customerOrderId);
    } catch (e) {
      throw e;
    }
  }
}
