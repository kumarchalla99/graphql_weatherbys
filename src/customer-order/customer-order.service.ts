import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import BadRequestException from "src/error/BadRequestException";
import { Repository } from "typeorm";
import {
  CustomerOrderEntity,
  CustomerOrderInput,
} from "./customer-order.model";

@Injectable()
export class CustomerOrderService {
  constructor(
    @InjectRepository(CustomerOrderEntity)
    private addressRepository: Repository<CustomerOrderEntity>
  ) {}

  async findAllCustomerOrders(): Promise<CustomerOrderEntity[]> {
    return await this.addressRepository.find();
  }

  async findCustomerOrder(orderInput): Promise<CustomerOrderEntity> {
    return await this.addressRepository.findOne({
      where: { ...orderInput },
    });
  }

  async addCustomerOrder(customerOrderInput: CustomerOrderInput) {
    // const adrs = await this.findCustomer({ email: customerOrderInput.dateOrdered });
    // if (adrs) throw new BadRequestException("user already exists");
    if (Object.values(customerOrderInput).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");

    const savedAdrs = await this.addressRepository.save(customerOrderInput);
    return savedAdrs;
  }

  async updateCustomerOrder(ordId: number, orderInput: CustomerOrderInput) {
    const order: CustomerOrderEntity = await this.findCustomerOrder({
      ordId: ordId,
    });
    if (!order) throw new BadRequestException("order not exists");
    if (Object.values(orderInput).every((x) => x === null || x === ""))
      throw new BadRequestException("please pass do not pass empty data");
    const res = await this.addressRepository.update(
      { ordId: ordId },
      orderInput
    );
    if (res) return res;
  }

  async deleteCustomerOrder(ordId: number) {
    const order: CustomerOrderEntity = await this.findCustomerOrder({
      ordId: ordId,
    });
    if (!order) throw new BadRequestException("user not exists");
    const res = await this.addressRepository.delete(order);
    if (res) return res;
  }
}
