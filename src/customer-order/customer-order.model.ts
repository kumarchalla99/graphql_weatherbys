import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { IsNotEmpty, ValidateIf } from "class-validator";
import { CustomerEntity } from "src/customer/customer.model";
import { ProductEntity } from "src/product/product.model";
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";

@ObjectType({ description: "customer_order" })
@Entity("customer_order")
export class CustomerOrderEntity {
  @Field({ nullable: false, name: "ordId" }) //if we want to map different name we can go with name
  @PrimaryGeneratedColumn({ name: "ord_id" })
  ordId: number;

  @Field({ nullable: false, name: "dateOrdered" })
  @Column({ name: "date_ordered", type: "timestamp" })
  dateOrdered: Date;

  @Field({ nullable: false, name: "orderedQty" })
  @Column({ name: "ordered_qty" })
  orderedQty: number;

  @Field({ nullable: false, name: "customerId" })
  @OneToMany((type) => CustomerEntity, (customer) => customer.custId)
  customerId: number;

  @Field({ nullable: false, name: "productId" })
  @OneToMany((type) => ProductEntity, (product) => product.pId)
  productId: number;
}

@InputType()
export class CustomerOrderInput {
  @ValidateIf((o) => o.otherProperty === "value")
  @Field({ nullable: false, name: "orderedQty" })
  @IsNotEmpty()
  orderedQty: number;

  @Field({ nullable: false, name: "dateOrdered" })
  @IsNotEmpty()
  dateOrdered: Date;

  @Field({ nullable: false, name: "customerId" })
  @IsNotEmpty()
  customerId: number;

  @Field({ nullable: false, name: "productId" })
  @IsNotEmpty()
  productId: number;
}
