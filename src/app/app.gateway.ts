// import {
//   SubscribeMessage,
//   WebSocketGateway,
//   OnGatewayInit,
//   WebSocketServer,
//   OnGatewayConnection,
//   OnGatewayDisconnect,
// } from "@nestjs/websockets";
// import { Socket, Server } from "socket.io";
// import { MessageEntity } from "src/messages/messages.entity";
// import { MessageService } from "src/messages/messages.service";

// @WebSocketGateway({
//   cors: {
//     origin: "*",
//   },
// })
// export class AppGateway
//   implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
// {
//   constructor(private chatService: MessageService) {}

//   @WebSocketServer() server: Server;

//   @SubscribeMessage("sendMessage")
//   async handleSendMessage(
//     client: Socket,
//     payload: MessageEntity
//   ): Promise<void> {
//     await this.chatService.saveMessage(payload);
//     this.server.emit("recMessage");
//     //Do stuffs
//   }

//   afterInit(server: Server) {
//     console.log(server);
//     //Do stuffs
//   }

//   handleDisconnect(client: Socket) {
//     console.log(`Disconnected: ${client.id}`);
//     //Do stuffs
//   }

//   handleConnection(client: Socket, ...args: any[]) {
//     console.log(`Connected ${client.id}`);
//     //Do stuffs
//   }
// }
