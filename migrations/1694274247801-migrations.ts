import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1694274247801 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`use chat_app;`);

    await queryRunner.query(`CREATE TABLE address ( 
      add_no int not null unique,
      id int NOT NULL AUTO_INCREMENT, 
      addr_line1 varchar(250) not null, 
      addr_line2 varchar(250), 
      city varchar(100) not null,  
      postcode int not null, 
      country varchar(50) not null, 
      primary key (id));`);

    await queryRunner.query(`CREATE TABLE customer (
       cust_id int NOT NULL AUTO_INCREMENT, 
       first_name varchar(100) not null, 
       last_name varchar(100) not null, 
       gender varchar(10) not null,  
       email varchar(100) not null, 
       landLine int not null, 
       mobile int not null,
       address_id int , 
       primary key (cust_id),
       FOREIGN KEY (address_id) REFERENCES address(id)
       );`);

    await queryRunner.query(`CREATE TABLE product (
      p_id int NOT NULL AUTO_INCREMENT,
      descrip_fon varchar(255) not null,
      unit_price int not null,
      available_qty int not null,
      primary key(p_id)
    );`);

    await queryRunner.query(`CREATE TABLE customer_order (
      ord_id int NOT NULL AUTO_INCREMENT,
      date_ordered timestamp NULL DEFAULT CURRENT_TIMESTAMP,
      ordered_qty int not null,
      customer_id int,
      product_id int,
      PRIMARY KEY (ord_id),
      FOREIGN KEY (customer_id) REFERENCES customer(cust_id),
      FOREIGN KEY (product_id) REFERENCES product(p_id)
    );`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query("SET FOREIGN_KEY_CHECKS=0;");
    await queryRunner.query("drop table address");
    await queryRunner.query("drop table customer");
    await queryRunner.query("drop table product");
    await queryRunner.query("drop table customer_order");
    await queryRunner.query("SET FOREIGN_KEY_CHECKS=1");
  }
}
