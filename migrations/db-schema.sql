CREATE DATABASE IF NOT EXISTS weatherbys;

CREATE TABLE address ( 
  add_no int not null unique,
  id int NOT NULL AUTO_INCREMENT, 
  addr_line1 varchar(250) not null, 
  addr_line2 varchar(250), 
  city varchar(100) not null,  
  postcode int not null, 
  country varchar(50) not null,
  primary key (id)
);

CREATE TABLE customer (
  cust_id int NOT NULL AUTO_INCREMENT, 
  first_name varchar(100) not null, 
  last_name varchar(100) not null, 
  gender varchar(10) not null,  
  email varchar(100) not null, 
  landLine int not null, 
  mobile int not null,
  address_id int , 
  primary key (cust_id),
  FOREIGN KEY (address_id) REFERENCES address(id)
);

CREATE TABLE product (
  p_id int NOT NULL AUTO_INCREMENT,
  descrip_fon varchar(255) not null,
  unit_price int not null,
  available_qty int not null,
  primary key(p_id)
);

CREATE TABLE customer_order (
  ord_id int NOT NULL AUTO_INCREMENT,
  date_ordered timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  ordered_qty int not null,
  customer_id int,
  product_id int,
  PRIMARY KEY (ord_id),
  FOREIGN KEY (customer_id) REFERENCES customer(cust_id),
  FOREIGN KEY (product_id) REFERENCES product(p_id)
);






